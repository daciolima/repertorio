def aprova_repertorio(modeladmin, request, queryset):
    queryset.update(aprovado=True)


def reprova_repertorio(modeladmin, request, queryset):
    queryset.update(aprovado=False)

aprova_repertorio.short_description = 'Aprovar repertório'

reprova_repertorio.short_description = 'Reprovar repertório'