from django.urls import path

from repertorio.api.viewsets import RepertorioViewSet
from repertorio.factory.generate_data import FactoryIntegrante



urlpatterns = [
    path('factory-integrante/', FactoryIntegrante.as_view(), name="repertorio"),
]



