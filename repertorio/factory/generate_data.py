
from django.http import HttpResponse
from django.shortcuts import render

# Create your views here.
import random
from django.views.generic import View
from faker import Faker
from repertorio.models import Integrante, Musica, Repertorio, Estilo, Tema

faker = Faker()


class FactoryIntegrante(View):

    def get(self, request):

        for _ in range(25):
            listInt = ['BT', 'GT', 'BS', 'VC', 'VL', 'TC', 'TS', 'NI']
            pessoa = Integrante.objects.create(nome=faker.name(), funcao=random.choice(listInt), descricao=faker.text(max_nb_chars=200, ext_word_list=None))


        listEstilo = ['Balada-Lenta', 'Balada-Animada', 'Rock', 'Clássica', 'Tradicional']
        lista = []
        for estilo in listEstilo:
            p = Estilo(nome=estilo, descricao=faker.text(max_nb_chars=200, ext_word_list=None))
            lista.append(p)

        Estilo.objects.bulk_create(lista)


        listaT = ['Ressurreição', 'Morte', 'Sangue', 'Perdão', 'Tradicional']
        listaTema = []
        for tema in listaT:
            p = Tema(nome=tema, descricao=faker.text(max_nb_chars=200, ext_word_list=None))
            listaTema.append(p)

        Tema.objects.bulk_create(listaTema)


        for _ in range(25):
            musica = Musica.objects.create(nome=faker.sentence(nb_words=6, variable_nb_words=True, ext_word_list=None),
                                           cantor=faker.name(), estilo_id=random.randrange(1, 5), tema_id=random.randrange(1, 5))





        return HttpResponse('Base de dados faker carregada')



