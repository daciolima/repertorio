# Generated by Django 2.2 on 2019-05-13 19:16

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('repertorio', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='integrante',
            name='foto',
            field=models.ImageField(blank=True, null=True, upload_to='foto_integrante'),
        ),
    ]
