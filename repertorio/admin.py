from django.contrib import admin

# Register your models here.
from .models import Integrante
from .models import Estilo
from .models import Tema
from .models import Musica
from .models import Repertorio

# Importa as actions a serem exibidas no Admin
from .actions import aprova_repertorio, reprova_repertorio


class IntegranteAdmin(admin.ModelAdmin):

    list_display = ('nome', 'funcao', 'descricao')

    search_fields = ['id', 'nome', 'descricao']


class MusicaAdmin(admin.ModelAdmin):
    search_fields = ['id', 'nome']


class RepertorioAdmin(admin.ModelAdmin):
    readonly_fields = ('data',)
    filter_horizontal = ['musica']
    raw_id_fields = ['integrante']
    list_display = ['id', 'culto', 'data', 'descricao', 'aprovado']
    actions = [aprova_repertorio, reprova_repertorio]






admin.site.register(Integrante, IntegranteAdmin)
admin.site.register(Estilo)
admin.site.register(Tema)
admin.site.register(Musica, MusicaAdmin)
admin.site.register(Repertorio, RepertorioAdmin)



