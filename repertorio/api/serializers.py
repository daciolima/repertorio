from rest_framework.fields import SerializerMethodField
from rest_framework.serializers import ModelSerializer

from repertorio.models import Musica, Integrante, Estilo, Repertorio, Tema


class EstiloSerializer(ModelSerializer):
    class Meta:
        model = Estilo
        fields = ['id', 'nome']

class TemaSerializer(ModelSerializer):
    class Meta:
        model = Tema
        fields = ['id', 'nome']

class MusicaSerializer(ModelSerializer):
    estilo = EstiloSerializer(read_only=True)
    tema = TemaSerializer(read_only=True)
    descricao_completa = SerializerMethodField()

    class Meta:
        model = Musica
        fields = ('id', 'nome', 'cantor', 'estilo', 'tema',  'descricao_completa', 'descricao_completa2')

    # Campo de retorno personalizado
    def get_descricao_completa(self, instance):
        return '%s - %s - %s ' % (instance.nome, instance.cantor, instance.estilo)



class IntegranteSerializer(ModelSerializer):
    class Meta:
        model = Integrante
        fields = ('id', 'nome', 'foto')

class RepertorioSerializer(ModelSerializer):
    musica = MusicaSerializer(many=True)
    integrante = IntegranteSerializer()

    class Meta:
        model = Repertorio
        fields = ('id', 'culto', 'data', 'descricao', 'musica', 'integrante')


    def cria_musica(self, musicas, repertorio):
        for musica in musicas:
            ms = Musica.objects.create(**musica)
            repertorio.musica.add(ms)

    def create(self, validated_data):
        #Carrega nas vari￿áveis os valores dos campos capturados
        musicas = validated_data['musica']
        integrante = validated_data['integrante']

        # Apaga as chaves
        del validated_data['musica']
        del validated_data['integrante']

        # Carega na vareavel todos os demais dados vindo da request
        repertorio = Repertorio.objects.create(**validated_data)
        self.cria_musica(musicas, repertorio)

        integ = Integrante.objects.create(**integrante)

        repertorio.integrante = integ

        repertorio.save()


        return repertorio





