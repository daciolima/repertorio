from django.http import HttpResponse
from rest_framework import permissions
from rest_framework.authentication import TokenAuthentication
from rest_framework.decorators import action
from rest_framework.filters import SearchFilter
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from repertorio.api.serializers import MusicaSerializer, IntegranteSerializer, EstiloSerializer, RepertorioSerializer, \
    TemaSerializer
from repertorio.models import Musica, Integrante, Estilo, Repertorio, Tema


class MusicaViewSet(ModelViewSet):

    #authentication_classes = (TokenAuthentication,)
    queryset = Musica.objects.all()
    serializer_class = MusicaSerializer

    # Filtro de busca do app django_filters
    #filter_fields = ('nome', 'cantor')

    # realiza busca por campo e campos de tabelas relacionadas
    filter_backends = (SearchFilter,)
    search_fields = ('nome', 'estilo__nome')

    # Busca por apenas um unico registro específico no banco(unique)
    lookup_field = 'nome'

    # Permissão somente para os admin
    # permission_classes = (IsAdminUser,)

    # Permissão a quem estiver com token
    # permission_classes = (IsAuthenticated,)
    #permission_classes = (permissions.IsAuthenticatedOrReadOnly,)


class IntegranteViewSet(ModelViewSet):

    queryset = Integrante.objects.all()
    serializer_class = IntegranteSerializer
    #permission_classes = (IsAuthenticated,)
    #permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

class EstiloViewSet(ModelViewSet):

    queryset = Estilo.objects.all()
    serializer_class = EstiloSerializer
    #permission_classes = (IsAuthenticated,)
    #permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

class TemaViewSet(ModelViewSet):
    queryset = Tema.objects.all()
    serializer_class = TemaSerializer
    #permission_classes = (IsAuthenticated,)
    #permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

class RepertorioViewSet(ModelViewSet):
    queryset = Repertorio.objects.all()
    serializer_class = RepertorioSerializer
    #permission_classes = (IsAuthenticated,)
    #permission_classes = (permissions.IsAuthenticatedOrReadOnly,)


### Sobrescrita dos métodos padrões da classe ModelViewSet
    def get_queryset(self):
        id = self.request.query_params.get('id', None)
        descricao = self.request.query_params.get('descricao', None)
        queryset = Repertorio.objects.all()

        if id:
            queryset = Repertorio.objects.filter(pk=id)

        if descricao:
            queryset = queryset.filter(descricao__icontains=descricao)

        return queryset


    def list(self, request, *args, **kwargs):
        return super(RepertorioViewSet, self).list(request, *args, **kwargs)

    def create(self, request, *args, **kwargs):
        return super(RepertorioViewSet, self).create(request, *args, **kwargs)

    def destroy(self, request, *args, **kwargs):
        return super(RepertorioViewSet, self).destroy(request, *args, **kwargs)

    def retrieve(self, request, *args, **kwargs):
        return super(RepertorioViewSet, self).retrieve(request, *args, **kwargs)

    def update(self, request, *args, **kwargs):
        return super(RepertorioViewSet, self).update(request, *args, **kwargs)
    
    def partial_update(self, request, *args, **kwargs):
        return super(RepertorioViewSet, self).partial_update(request, *args, **kwargs)
        


    # Action personalizada dependendo do pk para agir... api/recurso/1/pontuar
    @action(methods=['post','get'], detail=True)
    def pontuar(self, request, pk=None):
       pass

    # Action personalizada sem o pk para agir... api/recurso/teste
    @action(methods=['get'], detail=False)
    def teste(self, request):
       pass

    @action(methods=['post'], detail=True)
    def add_musicas(self, request, pk):
        musicas = request.data['ids']

        repertorio = Repertorio.objects.get(id=pk)
        repertorio.musica.set(musicas)

        repertorio.save()
        return HttpResponse('Ok')











