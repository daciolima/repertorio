from django.apps import AppConfig


class RepertorioConfig(AppConfig):
    name = 'repertorio'
