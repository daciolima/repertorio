from django.db import models

# Create your models here.
from django.utils.datetime_safe import datetime


class Integrante(models.Model):
    BATERISTA = 'BT'
    GUITARRISTA = 'GT'
    BAIXISTA = 'BS'
    VOCAL = 'VC'
    VIOLAO = 'VL'
    TECLADISTA = 'TC'
    TECNICO_SOM = 'TS'
    NAO_INFORMADO = 'NI'

    FUNCAO = (
        (BATERISTA, 'Baterista'),
        (GUITARRISTA, 'Guitarrista'),
        (BAIXISTA, 'Baixista'),
        (VOCAL, 'Vocal'),
        (VIOLAO, 'Violonista'),
        (TECLADISTA, 'Tecladista'),
        (TECNICO_SOM, 'Técnico de Som'),
        (NAO_INFORMADO, 'Não informado'),
    )


    nome = models.CharField(null=True, max_length=50)
    funcao = models.CharField(choices=FUNCAO, default=NAO_INFORMADO, max_length=2)
    descricao = models.TextField()
    foto = models.ImageField(upload_to='foto_integrante', null=True, blank=True)


    def __str__(self):
        return self.nome

class Estilo(models.Model):
    nome = models.CharField(null=True, max_length=50)
    descricao = models.TextField()

    DEFAULT_PK = 1

    def __str__(self):
        return self.nome

class Tema(models.Model):
    nome = models.CharField(null=True, max_length=50)
    descricao = models.TextField()

    DEFAULT_PK = 1

    def __str__(self):
        return self.nome

class Musica(models.Model):
    nome = models.CharField(null=True, max_length=50)
    cantor = models.CharField(max_length=50)
    estilo = models.ForeignKey(Estilo, null=True, default=Estilo.DEFAULT_PK, on_delete=models.PROTECT)
    tema = models.ForeignKey(Tema, null=True, default=Tema.DEFAULT_PK, on_delete=models.PROTECT)

    def __str__(self):
        return str(self.id) + ' - ' + self.nome + ' - ' + self.cantor + ' - ' + self.estilo.nome + ' - ' + self.tema.nome

    # Cria um campo de retorno personalizado, basta colocar o campo no serializer...
    @property
    def descricao_completa2(self):
        return '%s - %s - %s ' % (self.nome, self.cantor, self.estilo)

class Repertorio(models.Model):
    TERCA_NOITE = 'TN'
    QUARTA_NOITE = 'QN'
    SABADO_NOITE = 'SN'
    DOMINDO_MANHA ='DM'
    DOMINDO_NOITE = 'DN'
    OUTROS = 'OT'
    NAO_INFORMADO ='NI'

    CULTOS = (
        (TERCA_NOITE, 'Terça - Noite'),
        (QUARTA_NOITE, 'Quarta - Noite'),
        (SABADO_NOITE,'Sábado Noite'),
        (DOMINDO_MANHA, 'Domingo Manhã'),
        (DOMINDO_NOITE, 'Domingo Noite'),
        (OUTROS, 'Outros cultos'),
        (NAO_INFORMADO, 'Não Informado'),
    )
    culto = models.CharField(choices=CULTOS, default=NAO_INFORMADO, max_length=2)
    data = models.DateTimeField(auto_now_add=True)
    integrante = models.ForeignKey(Integrante, null=True, on_delete=models.CASCADE)
    musica = models.ManyToManyField(Musica, blank=True)
    descricao = models.TextField()
    aprovado = models.BooleanField(default=False)


    def __str__(self):
        return str(self.id) + ' ' + str(self.data) + ' ' + self.culto



