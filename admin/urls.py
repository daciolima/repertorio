from django.contrib import admin
from django.urls import path, include
from django.conf.urls.static import settings
from django.conf.urls.static import static
from rest_framework import routers
#from rest_framework.authtoken.views import obtain_auth_token

from repertorio.api.viewsets import MusicaViewSet, IntegranteViewSet, RepertorioViewSet, EstiloViewSet, TemaViewSet
from repertorio import urls as repertorio_urls
from rest_framework_simplejwt import views as jwt_views



router = routers.DefaultRouter()

router.register(r'integrantes', IntegranteViewSet)
router.register(r'estilos', EstiloViewSet)
router.register(r'temas', TemaViewSet)
router.register(r'musicas', MusicaViewSet)
router.register(r'repertorios', RepertorioViewSet)



urlpatterns = [
    #path('api-token-auth/', obtain_auth_token),
    path('api/', include(router.urls)),
    path('admin/', admin.site.urls),
    path('repertorio/', include(repertorio_urls)),
    path('api/token/', jwt_views.TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('api/token/refresh/', jwt_views.TokenRefreshView.as_view(), name='token_refresh'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
