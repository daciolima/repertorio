"""
WSGI config for admin project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/2.2/howto/deployment/wsgi/
"""

import os

from django.core.wsgi import get_wsgi_application


from dj_static import Cling

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'admin.settings')

#application = get_wsgi_application()

# Cling avalia se dados são estáticos ou é codigo python
application = Cling(get_wsgi_application())
